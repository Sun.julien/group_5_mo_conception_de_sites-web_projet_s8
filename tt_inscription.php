<?php
    session_start();

    $nom =  htmlentities($_POST['nom']);
    $prenom = htmlentities($_POST['prenom']);
    $email =  htmlentities($_POST['email']);
    $password = htmlentities($_POST['password']);
    $departement = htmlentities($_POST['id_departement']);

    $role = 1; // 1 pour élève
   

    $options = [
        'cost' => 12,
    ];

    include("connexionbdd.php");
    
    if ($stmt = $conn->prepare("INSERT INTO utilisateur(Nom,Prenom,Password,Mail,role,id_departement) VALUES (:Nom,:Prenom,MD5(:Password),:Mail,:role,:id_departement)")) {
        $stmt->bindParam(':Nom',$nom);
        $stmt->bindParam(':Prenom',$prenom);
        $stmt->bindParam(':Password',$password);
        $stmt->bindParam(':Mail',$email);
        $stmt->bindParam(':role',$role);
        $stmt->bindParam(':id_departement',$departement);

        //redirection vers sa dominante
        
        // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
        if($stmt->execute()) {
            $_SESSION['message'] = "Enregistrement réussi";

        } else {
            $_SESSION['message'] =  "Impossible d'enregistrer";
        }
    }
    // Redirection vers la page d'accueil par exemple :
    header('Location: confirmation.php');
?>
