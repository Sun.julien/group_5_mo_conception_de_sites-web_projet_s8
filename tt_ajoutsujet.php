<?php



    session_start();

    $titre =  htmlentities($_POST['titre']);
    $departement = htmlentities($_POST['departement']);
    $nbplace =  htmlentities($_POST['nbplace']);
    $resume = htmlentities($_POST['resume']);

    $options = [
        'cost' => 12,
    ];

    include("connexionbdd.php");
    
    if ($stmt = $conn->prepare("INSERT INTO sujet(Titre,Departement,NbPlace,Resume,role) VALUES (Titre,:Departement,:NbPlace,:Resume,:pdf)")) {
       //$passwordscript= hash_pw($password);
       //echo md5($password, PASSWORD_DEFAULT);
      // hash_pw($password);
        $stmt->bindParam(':Titre',$titre);
        $stmt->bindParam(':Departement',$departement);
        $stmt->bindParam(':NbPlace',$nbplace);
        $stmt->bindParam(':Resume',$resume);
        $stmt->bindParam(':pdf',$resume);
        
        // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
        if($stmt->execute()) {
            $_SESSION['message'] = "Enregistrement réussi";

        } else {
            $_SESSION['message'] =  "Impossible d'enregistrer";
        }
    }
    // Redirection vers la page d'accueil par exemple :
    header('Location: confirmation.php');
?>
